from datetime import datetime, timedelta


def gen_datas(n):
    curr = datetime.now()
    for i in range(n):
        curr += timedelta(days=5)
        yield curr


l = list(gen_datas(12))

print(l)
